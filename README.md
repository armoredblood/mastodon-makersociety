# MakerSociety Mastodon Project

Welcome to the MakerSociety mastodon server project!

The code here is used to describe and deploy a fully Infrastructure as Code (IAC) system that stands up and configures a server running the Mastodon project.

# Design Considerations
1. Minimize Cost
    * This project is funded mostly by me, with the occasional donation from others. The design should minimize recurring operational costs
1. Secure by default.
    * Take all precautions necessary to protect the server within the limitation of cost. Utilize server level security process to eliminate the need for a loadbalancer or web application firewall.
1. Low Maintenance
    * I've got a lot of other things I'd rather spend my time on than maintaining a single web app. The entire system must be as automated as possible and to minimize the need for manual interventions.
1. User Experience
    * All the above considerations *could* lead to a poor end user experience if the user is not taken into account when making decisions. The system should respond to user interaction in a reasonable timeframe. I'm not Facebook or Twitter nor do I have the wallet to provide a similar level of performance. You get what you get knowing that I'm doing my best as a tech professional to serve my community.

# Architecture Diagram
Here is a rough diagram of the intended implementation
```mermaid
flowchart TB
  subgraph AWS_Account
    direction TB
    subgraph VPC
      direction TB
      subgraph EC2
        direction TB  
        Mastodon_Server
      end
      subgraph RDS
        direction TB  
        db.t4g.micro
      end
      subgraph S3
        direction TB  
        Media_Storage_Bucket
      end
      EC2 --> Mastodon_Security_Group
      EC2 --> RDS
      EC2 --> S3
    end
  end
  subgraph Mastodon_Security_Group
    direction RL
        A(tcp 80 -> HTTP)
        B(tcp 443 -> HTTPS)
  end
  Mastodon_Security_Group --> Internet
```

# Important Files
The bulk of this repo is `AWS CDK` boilerplate most people won't be interested in seeing.

`mastodon/lib/mastodon-stack.ts` is where the bulk of the infrastructure code lives. 

`mastodon/lib/setup.sh` is the file that will do the bulk of the server setup after instnance initialization.

# Important Links
[Backing up your Server](https://docs.joinmastodon.org/admin/backups/)

[Mastodon Environment Variables](https://docs.joinmastodon.org/admin/config/)