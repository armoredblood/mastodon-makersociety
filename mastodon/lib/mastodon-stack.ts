import * as cdk from 'aws-cdk-lib';
import * as rds from 'aws-cdk-lib/aws-rds';
import * as s3 from 'aws-cdk-lib/aws-s3';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';

import * as path from 'path';

export class MastodonStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);

    const ec2 = cdk.aws_ec2;

    // create a vpc
    const vpc = new cdk.aws_ec2.Vpc(this, 'Vpc', {
      cidr: '10.0.0.0/16',
      natGateways: 0,
      subnetConfiguration: [
        {
          name: 'public-subnet-1',
          subnetType: ec2.SubnetType.PUBLIC,
          cidrMask: 24,
        },
        {
          name: 'isolated-subnet-1',
          subnetType: ec2.SubnetType.PRIVATE_ISOLATED,
          cidrMask: 28,
        }
      ]
    });

    // setup the instance security group
    const mastodonSecurityGroup = new ec2.SecurityGroup(this, 'instanceSG', {
      vpc,
      description: 'allow traffic only on certain ports',
      allowAllOutbound: false
    })

    // add specific ingress rules to the security group
    mastodonSecurityGroup.addIngressRule(ec2.Peer.anyIpv4(), ec2.Port.tcp(443), 'allow HTTPS traffic from everyone')
    mastodonSecurityGroup.addIngressRule(ec2.Peer.anyIpv4(), ec2.Port.tcp(80), 'allow HTTP traffic from everyone')

    // add specific egress rules to the security group
    mastodonSecurityGroup.addEgressRule(ec2.Peer.anyIpv4(), ec2.Port.tcp(443), 'allow HTTPS traffic to everyone')
    mastodonSecurityGroup.addEgressRule(ec2.Peer.anyIpv4(), ec2.Port.tcp(80), 'allow HTTP traffic to everyone')

    // if we need more storage, uncomment this block and configure the volume size
    // const rootVolume: cdk.aws_ec2.BlockDevice = {
    //   deviceName: '/dev/xvda',
    //   volume: ec2.BlockDeviceVolume.ebs(20)
    // }

    // define the ec2 instance the python app will run on
    const mastodonInstance = new cdk.aws_ec2.Instance(this, 'Instance', {
      vpc: vpc,
      vpcSubnets: vpc.selectSubnets({ subnetType: ec2.SubnetType.PUBLIC }),
      securityGroup: mastodonSecurityGroup,
      instanceType: new ec2.InstanceType('t4g.micro'),
      machineImage: cdk.aws_ec2.MachineImage.latestAmazonLinux(),
      userDataCausesReplacement: true,
      requireImdsv2: true
      // blockDevices: [rootVolume]
    });
 
    // upload setup script to s3
    const setupScript = new cdk.aws_s3_assets.Asset(this, 'setupScript', {
      path: path.join(__dirname, 'setup.sh'),
    });

    // grant ec2 instance access to setup script
    setupScript.grantRead(mastodonInstance.role)

    // download setup script to the ec2 instance
    const setupScriptPath = mastodonInstance.userData.addS3DownloadCommand({
      bucket: setupScript.bucket,
      bucketKey: setupScript.s3ObjectKey
    })

    // execute the setup script on the ec2 instance
    mastodonInstance.userData.addExecuteFileCommand({
      filePath: setupScriptPath,
      arguments: `${"argument1"}`
    })

    // create s3 bucket to host uploaded media
    const mediaBucket = new s3.Bucket(this, 'mediaBucket', {
      blockPublicAccess: s3.BlockPublicAccess.BLOCK_ALL,
      encryption: s3.BucketEncryption.S3_MANAGED
    })

    // grant the mastodon instance access to the media bucket
    mediaBucket.grantReadWrite(mastodonInstance)

    // create RDS PostgreSQL Database
    const dbInstance = new rds.DatabaseInstance(this, 'db-instance', {
      vpc,
      vpcSubnets: vpc.selectSubnets({ subnetType: ec2.SubnetType.PRIVATE_ISOLATED }),
      engine: rds.DatabaseInstanceEngine.postgres({
        version: rds.PostgresEngineVersion.VER_14_3,
      }),
      instanceType: new ec2.InstanceType('db.t4g.micro'),
      credentials: rds.Credentials.fromGeneratedSecret('postgres'),
      multiAz: false, //should be true for High Availability Production use cases, but will cost more
      storageEncrypted: true,
      allocatedStorage: 50,
      maxAllocatedStorage: 500,
      allowMajorVersionUpgrade: false,
      autoMinorVersionUpgrade: true,
      backupRetention: cdk.Duration.days(7),
      deleteAutomatedBackups: true,
      removalPolicy: cdk.RemovalPolicy.DESTROY,
      deletionProtection: true,
      publiclyAccessible: false,
      port: 5432
    });

    // allow connections to the database from the mastodon instance security group
    dbInstance.connections.allowFrom(mastodonSecurityGroup, ec2.Port.tcp(5432));

    // output the RDS endpoint URL
    new cdk.CfnOutput(this, 'dbEndpoint', {
      value: dbInstance.instanceEndpoint.hostname,
    });

    // output the arn of the rds user secret
    new cdk.CfnOutput(this, 'secretName', {
      value: (typeof dbInstance.secret?.secretArn !== "undefined") ? dbInstance.secret?.secretArn : "undefined"
    });

  }
}
